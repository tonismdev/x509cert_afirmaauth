/*
 * Copyright 2016 Analytical Graphics, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package es.caib.autenticacio.afirma;

import java.security.cert.X509Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateException;
import java.io.ByteArrayInputStream;
import java.util.Base64;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.fundaciobit.pluginsib.validatecertificate.ICertificatePlugin;
import org.fundaciobit.pluginsib.validatecertificate.ResultatValidacio;
import org.fundaciobit.pluginsib.validatecertificate.afirmacxf.AfirmaCxfCertificatePlugin;
import org.fundaciobit.pluginsib.core.utils.PluginsManager;
import org.jboss.resteasy.spi.ResteasyProviderFactory;
import org.keycloak.authentication.AuthenticationFlowContext;
import org.keycloak.authentication.authenticators.browser.AbstractUsernameFormAuthenticator;
import org.keycloak.events.Details;
import org.keycloak.events.Errors;
import org.keycloak.forms.login.LoginFormsProvider;
import org.keycloak.models.ModelDuplicateException;
import org.keycloak.models.UserModel;
import org.keycloak.models.utils.FormMessage;
import org.keycloak.models.utils.KeycloakModelUtils;
import org.keycloak.representations.idm.OAuth2ErrorRepresentation;
import org.keycloak.utils.MediaType;





	/**
	 * @author <a href="mailto:santich@dgtic.caib.es">Salvador Antich Sastre</a>
	 * @author <a href="mailto:asanchez@imas.conselldemallorca.net">Toni Sanchez Magraner</a>
	 * @author <a href="mailto:pnalyvayko@agi.com">Peter Nalyvayko</a>
	 * @version $Revision: 3 $
	 *
	 */
	public class AfirmaClientCertificateAuthenticator extends AbstractAfirmaClientCertificateAuthenticator{
		
	    @Override
	    public void close() {

	    }

	    @Override
	    public void authenticate(AuthenticationFlowContext context) {
	        try {
	            dumpContainerAttributes(context);
	            X509Certificate[] certs = getCertificateChain(context);
	            
	            // Si l'usuari no té certificats es bota l'autenticació per certificat.
	            if (certs == null || certs.length == 0) {
	            	
	            	// Primer comprovam que el certificat el proporcioni el proxy (apache)
		            HttpServletRequest httpServletRequest = ResteasyProviderFactory.getContextData(HttpServletRequest.class);

					// A efectes de treure per log les capçaleres de la petició http
					Enumeration<String> llistaCapcaleres = httpServletRequest.getHeaderNames();
					while (llistaCapcaleres.hasMoreElements()) {
		            	String capcalera = llistaCapcaleres.nextElement();
						logger.info("Valor capçalera " + capcalera + " ---> " + httpServletRequest.getHeader(capcalera));
		            }

					// A efectes de treure per log els atributs de la petició http
		            Enumeration<String> llistaAtributs = httpServletRequest.getAttributeNames();
		            while (llistaAtributs.hasMoreElements()) {
		            	String atribut = llistaAtributs.nextElement();
		            	logger.info("Atributs de la request: ---> " + atribut);
		            }
		            

		            //Object certObj = httpServletRequest.getAttribute("javax.servlet.request.X509Certificate");
					certs[0] = parseCertificate(httpServletRequest.getHeader("SSL_CLIENT_CERT"));

	            	if (certs == null || certs.length ==0) {
		                // No x509 client cert, fall through and
		                // continue processing the rest of the authentication flow
		                logger.info("[X509ClientCertificateAuthenticator:authenticate] x509 client certificate is not available for mutual SSL.");
		                context.attempted();
		                return;
	            	}else{
						logger.info("Certificat X509 provinent de Apache:" + certs[0]);
					}
	            }
				logger.info("L'usuari té certificats: " + certs[0]);

	            AfirmaAuthenticatorConfigModel config = null;
				

	            if (context.getAuthenticatorConfig() != null && context.getAuthenticatorConfig().getConfig() != null) {
	                config = new AfirmaAuthenticatorConfigModel(context.getAuthenticatorConfig());
	            }
	            if (config == null) {
	                logger.warn("[X509ClientCertificateAuthenticator:authenticate] x509 Client Certificate Authentication configuration is not available.");
	                context.challenge(createInfoResponse(context, "X509 client authentication has not been configured yet"));
	                context.attempted();
	                return;
	            }
	            
	            Integer validationMode = config.getValidationMode();
	            String endpoint = config.getEndpoint();
	            String applicationId = config.getAplicationId();
	            String username = config.getUserName();
	            String password = config.getPassword();
	            
	            String keystorePath = config.getKeystorePath();
	            String keystoreType = config.getKeystoreType();	            
	            String keystorePwd = config.getKeystorePassword();
	            String keystoreAlias = config.getKeystoreAlias();
	            String keystoreCertPwd = config.getKeystoreCertPassword();	            
	            
	            logger.info(String.format("@firma: validationMode %s \n endpoint %s \n applicationId %s \n username %s \n password %s \n keystorePath %s \n keystoreType %s \n keystorePwd %s \n keystoreAlias %s \n keystoreCertPwd %s \n",validationMode, endpoint,applicationId,username,password,keystorePath,keystoreType,keystorePwd,keystoreAlias,keystoreCertPwd));
	            
	            Properties pluginProp = new Properties();
	            pluginProp.put("plugins.certificate.afirma.validationmode", validationMode);
	            pluginProp.put("plugins.certificate.afirma.endpoint", endpoint);
	            pluginProp.put("plugins.certificate.afirma.applicationid", applicationId);
	            pluginProp.put("plugins.certificate.afirma.authorization.username", username);
	            pluginProp.put("plugins.certificate.afirma.authorization.password", password);
	            pluginProp.put("plugins.certificate.afirma.authorization.ks.path", keystorePath);	
	            pluginProp.put("plugins.certificate.afirma.authorization.ks.type", keystoreType);
	            pluginProp.put("plugins.certificate.afirma.authorization.ks.password", keystorePwd);
	            pluginProp.put("plugins.certificate.afirma.authorization.ks.cert.alias", keystoreAlias);
	            pluginProp.put("plugins.certificate.afirma.authorization.ks.cert.password", keystoreCertPwd);
	            
	            String nif = null;
	            try {
		            final Class<?> c = AfirmaCxfCertificatePlugin.class;	            
		            ICertificatePlugin plugin = (ICertificatePlugin) PluginsManager.instancePluginByClass(c, "", pluginProp);
					logger.info("Es realitza la cridada al webservice de validació del certificat");
					ResultatValidacio resultValid = plugin.getInfoCertificate(certs[0]);
					logger.info("Retorn de la cridada al webservice de validació emprant pluginsib");
					nif = resultValid.getInformacioCertificat().getNifResponsable();
	            }catch (ModelDuplicateException e) {
	            	logger.modelDuplicateException(e);
	                String errorMessage = "X509 certificate authentication's failed.";
	                // TODO is calling form().setErrors enough to show errors on login screen?
	                context.challenge(createErrorResponse(context, certs[0].getSubjectDN().getName(),
	                        errorMessage, "Error de comunicació amb @firma"));
	                context.attempted();
	                return;
	            }

	            UserModel user;
	            try {
	                context.getEvent().detail(Details.USERNAME, nif);
	                context.getAuthenticationSession().setAuthNote(AbstractUsernameFormAuthenticator.ATTEMPTED_USERNAME, nif);
	                user = KeycloakModelUtils.findUserByNameOrEmail(context.getSession(), context.getRealm(), nif.trim());
	                //user = getUserIdentityToModelMapper(config).find(context, nif);
	            }
	            catch(ModelDuplicateException e) {
	                logger.modelDuplicateException(e);
	                String errorMessage = "X509 certificate authentication's failed.";
	                // TODO is calling form().setErrors enough to show errors on login screen?
	                context.challenge(createErrorResponse(context, certs[0].getSubjectDN().getName(),
	                        errorMessage, e.getMessage()));
	                context.attempted();
	                return;
	            }

	            if (invalidUser(context, user)) {
	                // TODO use specific locale to load error messages
	                String errorMessage = "X509 certificate authentication's failed.";
	                // TODO is calling form().setErrors enough to show errors on login screen?
	                context.challenge(createErrorResponse(context, certs[0].getSubjectDN().getName(),
	                        errorMessage, "Invalid user"));
	                context.attempted();
	                return;
	            }

	            if (!userEnabled(context, user)) {
	                // TODO use specific locale to load error messages
	                String errorMessage = "X509 certificate authentication's failed.";
	                // TODO is calling form().setErrors enough to show errors on login screen?
	                context.challenge(createErrorResponse(context, certs[0].getSubjectDN().getName(),
	                        errorMessage, "User is disabled"));
	                context.attempted();
	                return;
	            }
	            if (context.getRealm().isBruteForceProtected()) {
	                if (context.getProtector().isTemporarilyDisabled(context.getSession(), context.getRealm(), user)) {
	                    context.getEvent().user(user);
	                    context.getEvent().error(Errors.USER_TEMPORARILY_DISABLED);
	                    // TODO use specific locale to load error messages
	                    String errorMessage = "X509 certificate authentication's failed.";
	                    // TODO is calling form().setErrors enough to show errors on login screen?
	                    context.challenge(createErrorResponse(context, certs[0].getSubjectDN().getName(),
	                            errorMessage, "User is temporarily disabled. Contact administrator."));
	                    context.attempted();
	                    return;
	                }
	            }
	            context.setUser(user);

	            context.success();
	        }
	        catch(Exception e) {
	            logger.errorf("[X509ClientCertificateAuthenticator:authenticate] Exception: %s \n", e.getMessage());
	            context.attempted();
	        }
	    }
	    
	    /** @autor: Antoni Sànchez Magraner*/
	    /* Agafat de keycloak/services/src/main/java/org/keycloak/authentication/authenticators/directgrant/ */
	    public Response errorResponse(int status, String error, String errorDescription) {
	        OAuth2ErrorRepresentation errorRep = new OAuth2ErrorRepresentation(error, errorDescription);
	        return Response.status(status).entity(errorRep).type(MediaType.APPLICATION_JSON_TYPE).build();
	    }
	    

	    private Response createErrorResponse(AuthenticationFlowContext context,
	                                         String subjectDN,
	                                         String errorMessage,
	                                         String ... errorParameters) {

	        return createResponse(context, subjectDN, false, errorMessage, errorParameters);
	    }

	    private Response createSuccessResponse(AuthenticationFlowContext context, String subjectDN) {
	        return createResponse(context, subjectDN, true, null, null);
	    }

	    private Response createResponse(AuthenticationFlowContext context,
	                                         String subjectDN,
	                                         boolean isUserEnabled,
	                                         String errorMessage,
	                                         Object[] errorParameters) {

	        LoginFormsProvider form = context.form();
	        if (errorMessage != null && errorMessage.trim().length() > 0) {
	            List<FormMessage> errors = new LinkedList<>();

	            errors.add(new FormMessage(errorMessage));
	            if (errorParameters != null) {

	                for (Object errorParameter : errorParameters) {
	                    if (errorParameter == null) continue;
	                    for (String part : errorParameter.toString().split("\n")) {
	                        errors.add(new FormMessage(part));
	                    }
	                }
	            }
	            form.setErrors(errors);
	        }

	        MultivaluedMap<String,String> formData = new MultivaluedHashMap<>();
	        formData.add("username", context.getUser() != null ? context.getUser().getUsername() : "unknown user");
	        formData.add("subjectDN", subjectDN);
	        formData.add("isUserEnabled", String.valueOf(isUserEnabled));

	        form.setFormData(formData);

	        return form.createX509ConfirmPage();
	    }

	    private void dumpContainerAttributes(AuthenticationFlowContext context) {

	        Enumeration<String> attributeNames = context.getHttpRequest().getAttributeNames();
	        while(attributeNames.hasMoreElements()) {
	            String a = attributeNames.nextElement();
	            //Object v = context.getHttpRequest().getAttribute(a);
	            logger.tracef("[X509ClientCertificateAuthenticator:dumpContainerAttributes] \"%s \n\"", a);
	        }
	    }

	    private boolean userEnabled(AuthenticationFlowContext context, UserModel user) {
	        if (!user.isEnabled()) {
	            context.getEvent().user(user);
	            context.getEvent().error(Errors.USER_DISABLED);
	            return false;
	        }
	        return true;
	    }

	    private boolean invalidUser(AuthenticationFlowContext context, UserModel user) {
	        if (user == null) {
	            context.getEvent().error(Errors.USER_NOT_FOUND);
	            return true;
	        }
	        return false;
	    }

	    @Override
	    public void action(AuthenticationFlowContext context) {
	        MultivaluedMap<String, String> formData = context.getHttpRequest().getDecodedFormParameters();
	        if (formData.containsKey("cancel")) {
	            context.clearUser();
	            context.attempted();
	            return;
	        }
	        if (context.getUser() != null) {
	            //recordX509CertificateAuditDataViaContextEvent(context);
	            context.success();
	            return;
	        }
	        context.attempted();
	    }

		public static X509Certificate parseCertificate(String pem) throws CertificateException{
			// S'elimina la capçalera i el peu: "--- BEGIN CERTIFICATE ---" i "--- END CERTIFICATE ---"
			//String cert = pem.replaceAll("-----BEGIN CERTIFICATE-----", "").replaceAll("-----END CERTIFICATE-----", "");
			logger.info("Certificat String: "+ pem);
			InputStream certStream = new ByteArrayInputStream(Base64.getDecoder().decode(pem));
			/*logger.info("certificat: ---> " + cert);
			byte [] decoded = Base64.getMimeDecoder().decode(cert);*/
			//return (X509Certificate)CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(decoded));
			logger.info("Cert Stream: " + certStream);
			return (X509Certificate)CertificateFactory.getInstance("X.509").generateCertificate(certStream);
		}
	}
