package es.caib.autenticacio.afirma;

import static es.caib.autenticacio.afirma.AbstractAfirmaClientCertificateAuthenticator.*;

import org.keycloak.models.AuthenticatorConfigModel;

public class AfirmaAuthenticatorConfigModel extends AuthenticatorConfigModel {

	public enum ValidationMode {
            VALIDACIO_SIMPLE (0, MAPPING_VALIDACIO_SIMPLE),
            VALIDACIO_AMB_REVOCACIO (1, MAPPING_VALIDACIO_AMB_REVOCACIO),
            VALIDACIO_CADADENA (2, MAPPING_VALIDACIO_CADENA);
		
		private String name;
		private Integer value;
		
		ValidationMode(int value, String name) {
		   if (value < 0 || value > 2)
                throw new IllegalArgumentException("value");
            if (name == null || name.trim().length() == 0)
                throw new IllegalArgumentException("name");
            this.value = value;
            this.name = name.trim();
        }
		
		public int getInt() { return this.value; }
        public String getName() {  return this.name; }
        
        static public ValidationMode parse(String name) throws IllegalArgumentException, IndexOutOfBoundsException {
            if (name == null || name.trim().length() == 0)
                throw new IllegalArgumentException("name");

            for (ValidationMode value : ValidationMode.values()) {
                if (value.getName().equalsIgnoreCase(name))
                    return value;
            }
            throw new IndexOutOfBoundsException("name");
        }
        
        static public ValidationMode fromValue(int value) throws IndexOutOfBoundsException {
            if (value < 0 || value > 8)
                throw new IndexOutOfBoundsException("value");
            for (ValidationMode v : ValidationMode.values())
                if (v.getInt() == value)
                    return v;
            throw new IndexOutOfBoundsException("value");
        }
    };
    
    public AfirmaAuthenticatorConfigModel(AuthenticatorConfigModel model) {
        this.setAlias(model.getAlias());
        this.setId(model.getId());
        this.setConfig(model.getConfig());
    }
    
    public AfirmaAuthenticatorConfigModel() {

    }
     
    public Integer getValidationMode() {
    	String validationMode = getConfig().getOrDefault(MAPPING_VALIDATION_MODE_SELECTION, MAPPING_VALIDACIO_SIMPLE);
        return ValidationMode.parse(validationMode).getInt();
    }

    public AfirmaAuthenticatorConfigModel setValidationMode(Integer validationMode) {
        if (validationMode != null) {        	
            getConfig().put(MAPPING_VALIDATION_MODE_SELECTION, ValidationMode.fromValue(validationMode).getName());
        } else {
            getConfig().remove(MAPPING_VALIDATION_MODE_SELECTION);
        }
        return this;
    }    
    
    public String getEndpoint() {
        return getConfig().getOrDefault(ENDPOINT_ATTRIBUTE_MAPPER, null);
    }
    
    public AfirmaAuthenticatorConfigModel setEndpoint(String endpoint) {
        if (endpoint != null) {
            getConfig().put(ENDPOINT_ATTRIBUTE_MAPPER, endpoint);
        } else {
            getConfig().remove(ENDPOINT_ATTRIBUTE_MAPPER);
        }
        return this;
    }
    
    public String getAplicationId() {
        return getConfig().getOrDefault(APPLICATIONID_ATTRIBUTE_MAPPER, null);
    }

    public AfirmaAuthenticatorConfigModel setAplicationId(String applicationId) {
        if (applicationId != null) {
            getConfig().put(APPLICATIONID_ATTRIBUTE_MAPPER, applicationId);
        } else {
            getConfig().remove(APPLICATIONID_ATTRIBUTE_MAPPER);
        }
        return this;
    }
    
    public String getUserName() {
        return getConfig().getOrDefault(AUTHORIZATION_USERNAME_MAPPER, null);
    }

    public AfirmaAuthenticatorConfigModel setUserName(String userName) {
        if (userName != null) {
            getConfig().put(AUTHORIZATION_USERNAME_MAPPER, userName);
        } else {
            getConfig().remove(AUTHORIZATION_USERNAME_MAPPER);
        }
        return this;
    }
    
    public String getPassword() {
        return getConfig().getOrDefault(AUTHORIZATION_PASSWORD_MAPPER, null);
    }

    public AfirmaAuthenticatorConfigModel setPassword(String password) {
        if (password != null) {
            getConfig().put(AUTHORIZATION_PASSWORD_MAPPER, password);
        } else {
            getConfig().remove(AUTHORIZATION_PASSWORD_MAPPER);
        }
        return this;
    }

    public String getKeystoreType() {
        return getConfig().getOrDefault(AUTHORIZATION_KS_TYPE, null);
    }

    public AfirmaAuthenticatorConfigModel setKeystoreType(String keystoreType) {
        if (keystoreType != null) {
            getConfig().put(AUTHORIZATION_KS_TYPE, keystoreType);
        } else {
            getConfig().remove(AUTHORIZATION_KS_TYPE);
        }
        return this;
    }
		
    public String getKeystorePath() {
        return getConfig().getOrDefault(AUTHORIZATION_KS_PATH, null);
    }

    public AfirmaAuthenticatorConfigModel setKeystorePath(String keystorePath) {
        if (keystorePath != null) {
            getConfig().put(AUTHORIZATION_KS_PATH, keystorePath);
        } else {
            getConfig().remove(AUTHORIZATION_KS_PATH);
        }
        return this;
    }

    public String getKeystorePassword() {
        return getConfig().getOrDefault(AUTHORIZATION_KS_PASSWORD, null);
    }

    public AfirmaAuthenticatorConfigModel setKeystorePassword(String keystorePassword) {
        if (keystorePassword != null) {
            getConfig().put(AUTHORIZATION_KS_PASSWORD, keystorePassword);
        } else {
            getConfig().remove(AUTHORIZATION_KS_PASSWORD);
        }
        return this;
    }
	
    public String getKeystoreAlias() {
        return getConfig().getOrDefault(AUTHORIZATION_KS_CERT_ALIAS, null);
    }

    public AfirmaAuthenticatorConfigModel setKeystoreAlias(String keystoreAlias) {
        if (keystoreAlias != null) {
            getConfig().put(AUTHORIZATION_KS_CERT_ALIAS, keystoreAlias);
        } else {
            getConfig().remove(AUTHORIZATION_KS_CERT_ALIAS);
        }
        return this;
    }
    
    public String getKeystoreCertPassword() {
        return getConfig().getOrDefault(AUTHORIZATION_KS_CERT_PASSWORD, null);
    }

    
    public AfirmaAuthenticatorConfigModel setKeystoreCertPassword(String keystoreCertPassword) {
        if (keystoreCertPassword != null) {
            getConfig().put(AUTHORIZATION_KS_CERT_PASSWORD, keystoreCertPassword);
        } else {
            getConfig().remove(AUTHORIZATION_KS_CERT_PASSWORD);
        }
        return this;
    }
    
    /*
    public AfirmaAuthenticatorConfigModel setKeystorePassw(String keystorePassw) {
        if (keystorePassw != null) {
            getConfig().put(AUTHORIZATION_KS_CERT_PASSWORD, keystorePassw);
        } else {
            getConfig().remove(AUTHORIZATION_KS_CERT_PASSWORD);
        }
        return this;
    }*/

}
