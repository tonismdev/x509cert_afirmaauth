package es.caib.autenticacio.afirma;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.keycloak.Config.Scope;
import org.keycloak.authentication.Authenticator;
import org.keycloak.authentication.AuthenticatorFactory;
import org.keycloak.models.AuthenticationExecutionModel.Requirement;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.services.ServicesLogger;

import static es.caib.autenticacio.afirma.AbstractAfirmaClientCertificateAuthenticator.*;
import static java.util.Arrays.asList;
import static org.keycloak.provider.ProviderConfigProperty.STRING_TYPE;

public abstract class AbstractAfirmaClientCertificateAuthenticatorFactory implements AuthenticatorFactory{
/*
 * @author <a href="mailto:santich@dgtic.caib.es">Salvador Antich Sastre</a>
 */
	protected static ServicesLogger logger = ServicesLogger.LOGGER;

	private static final String[] mappingValidationMode = {
			MAPPING_VALIDACIO_SIMPLE,
			MAPPING_VALIDACIO_AMB_REVOCACIO,
			MAPPING_VALIDACIO_CADENA
	};

	protected static final List<ProviderConfigProperty> configProperties;
    static {
        List<String> mappingValidationModes = new LinkedList<>();
        Collections.addAll(mappingValidationModes, mappingValidationMode);
        ProviderConfigProperty mappingMethodList = new ProviderConfigProperty();
        mappingMethodList.setType(ProviderConfigProperty.LIST_TYPE);
        mappingMethodList.setName(MAPPING_VALIDATION_MODE_SELECTION);
        mappingMethodList.setLabel("Mode de validació");
        mappingMethodList.setHelpText("Mode de validació.");
        mappingMethodList.setDefaultValue(mappingValidationMode[0]);
        mappingMethodList.setOptions(mappingValidationModes);
        
        ProviderConfigProperty endpoint = new ProviderConfigProperty();
        endpoint.setType(STRING_TYPE);
        endpoint.setName(ENDPOINT_ATTRIBUTE_MAPPER);
        endpoint.setLabel("Url servei");
        endpoint.setDefaultValue("");
        endpoint.setHelpText("Url de l'endpoint del servei @firma");
        
        ProviderConfigProperty applicationId = new ProviderConfigProperty();
        applicationId.setType(STRING_TYPE);
        applicationId.setName(APPLICATIONID_ATTRIBUTE_MAPPER);
        applicationId.setLabel("ID Aplicació");
        applicationId.setDefaultValue("");
        applicationId.setHelpText("Codi de l'aplicacio donada d'alta dins @firma");

        ProviderConfigProperty username = new ProviderConfigProperty();
        username.setType(STRING_TYPE);
        username.setName(AUTHORIZATION_USERNAME_MAPPER);
        username.setLabel("Usuari");
        username.setDefaultValue("");
        username.setHelpText("Usuari de connexió al servei");
        
        ProviderConfigProperty password = new ProviderConfigProperty();
        password.setType(STRING_TYPE);
        password.setName(AUTHORIZATION_PASSWORD_MAPPER);
        password.setLabel("Password");
        password.setDefaultValue("");
        password.setHelpText("Password de connexió al servei");
        
        ProviderConfigProperty keystoreType = new ProviderConfigProperty();
        keystoreType.setType(STRING_TYPE);
        keystoreType.setName(AUTHORIZATION_KS_TYPE);
        keystoreType.setLabel("Tipus");
        keystoreType.setDefaultValue("JKS");
        keystoreType.setHelpText("Tipus de keystore");
        
        ProviderConfigProperty keystorePath = new ProviderConfigProperty();
        keystorePath.setType(STRING_TYPE);
        keystorePath.setName(AUTHORIZATION_KS_PATH);
        keystorePath.setLabel("Ruta");
        keystorePath.setDefaultValue("");
        keystorePath.setHelpText("Ruta del keystore on hi ha el certificat");
        
        ProviderConfigProperty keystoreAlias = new ProviderConfigProperty();
        keystoreAlias.setType(STRING_TYPE);
        keystoreAlias.setName(AUTHORIZATION_KS_CERT_ALIAS);
        keystoreAlias.setLabel("Alias del certificat");
        keystoreAlias.setDefaultValue("");
        keystoreAlias.setHelpText("Alias del certificat a emprar per autenticar-se contra el servei");
        
        ProviderConfigProperty keystoreCertPwd = new ProviderConfigProperty();
        keystoreCertPwd.setType(STRING_TYPE);
        keystoreCertPwd.setName(AUTHORIZATION_KS_CERT_PASSWORD);
        keystoreCertPwd.setLabel("Password del certificat");
        keystoreCertPwd.setDefaultValue("");
        keystoreCertPwd.setHelpText("Password del certificat");

        ProviderConfigProperty keystorePwd = new ProviderConfigProperty();
        keystorePwd.setType(STRING_TYPE);
        keystorePwd.setName(AUTHORIZATION_KS_PASSWORD);
        keystorePwd.setLabel("Password del keystore");
        keystorePwd.setDefaultValue("");
        keystorePwd.setHelpText("Password del keystore");

        configProperties = asList(
        		mappingMethodList,
        		endpoint,
        		applicationId,
        		username,
        		password,
        		keystoreType,
        		keystorePath,
        		keystoreAlias,
        		keystoreCertPwd,
                keystorePwd
        		);
    }
       
	@Override
	public List<ProviderConfigProperty> getConfigProperties() {
		return configProperties;
	}
	
	@Override
	public String getReferenceCategory() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean isConfigurable() {
		return true;
	}
	
	@Override
	public boolean isUserSetupAllowed() {		
		return false;
	}
	
	@Override
	public void init(Scope config) {
	}
	
	@Override
	public void postInit(KeycloakSessionFactory factory) {
	}
	
	@Override
	public void close() {	
	}

}
