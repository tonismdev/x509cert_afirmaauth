package es.caib.autenticacio.afirma;

import org.keycloak.authentication.Authenticator;
import org.keycloak.models.AuthenticationExecutionModel;
import org.keycloak.models.KeycloakSession;

public class AfirmaClientCertificateAuthenticatorFactory extends AbstractAfirmaClientCertificateAuthenticatorFactory {
    // public static final String PROVIDER_ID = "auth-x509-client-username-form";
	public static final String PROVIDER_ID = "auth-x509-afirma";
    public static final AfirmaClientCertificateAuthenticator SINGLETON =
            new AfirmaClientCertificateAuthenticator();
    
    @Override
    public String getHelpText() {
        return "Valida el certificat contra @firma";
    }

    @Override
    public String getDisplayType() {
        return "X509@firma";
    }

    @Override
    public AuthenticationExecutionModel.Requirement[] getRequirementChoices() {
    	AuthenticationExecutionModel.Requirement[] REQUIREMENT_CHOICES = {
            AuthenticationExecutionModel.Requirement.REQUIRED,
            AuthenticationExecutionModel.Requirement.ALTERNATIVE,
            AuthenticationExecutionModel.Requirement.DISABLED};
        return REQUIREMENT_CHOICES;
    }


    @Override
    public Authenticator create(KeycloakSession session) {
        return SINGLETON;
    }

    @Override
    public String getId() {
        return PROVIDER_ID;
    }
}
