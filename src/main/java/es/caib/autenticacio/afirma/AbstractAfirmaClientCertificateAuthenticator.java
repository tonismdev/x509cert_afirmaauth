package es.caib.autenticacio.afirma;

import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;

import javax.ws.rs.core.Response;

import org.keycloak.authentication.AuthenticationFlowContext;
import org.keycloak.authentication.Authenticator;
import org.keycloak.forms.login.LoginFormsProvider;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.services.ServicesLogger;
import org.keycloak.services.x509.X509ClientCertificateLookup;

public abstract class AbstractAfirmaClientCertificateAuthenticator implements Authenticator {
	protected static ServicesLogger logger = ServicesLogger.LOGGER;
	
	public static final String MAPPING_VALIDATION_MODE_SELECTION = "afirma-cert-auth.mapping-validation-mode-selection";
	public static final String MAPPING_VALIDACIO_SIMPLE = "Validació simple";
	public static final String MAPPING_VALIDACIO_AMB_REVOCACIO = "Validació amb revocació";	
	public static final String MAPPING_VALIDACIO_CADENA = "Validació cadena";
	public static final String ENDPOINT_ATTRIBUTE_MAPPER = "afirma-cert-auth.endpoint";
	public static final String APPLICATIONID_ATTRIBUTE_MAPPER = "afirma-cert-auth.application.id";
	
	public static final String AUTHORIZATION_USERNAME_MAPPER = "afirma-cert-auth.username";
	public static final String AUTHORIZATION_PASSWORD_MAPPER = "afirma-cert-auth.password";
	
	public static final String AUTHORIZATION_KS_TYPE = "afirma-cert-auth.keystore-type";			
	public static final String AUTHORIZATION_KS_PATH = "afirma-cert-auth.keystore-path";
	public static final String AUTHORIZATION_KS_PASSWORD = "afirma-cert-auth.keystore-password";
	public static final String AUTHORIZATION_KS_CERT_ALIAS = "afirma-cert-auth.keystore-cert-alias";
	public static final String AUTHORIZATION_KS_CERT_PASSWORD = "afirma-cert-auth.keystore-cert-password";
/*	
    protected static class CertificateValidatorConfigBuilder {

        static CertificateValidator.CertificateValidatorBuilder fromConfig(KeycloakSession session, AfirmaAuthenticatorConfigModel config) throws Exception {
        	final Class<?> c = AfirmaCxfCertificatePlugin.class;	            
            Properties pluginProp = new Properties();
            
			ICertificatePlugin plugin = (ICertificatePlugin) PluginsManager.instancePluginByClass(c, "", pluginProp );
            
            CertificateValidator.CertificateValidatorBuilder builder = new CertificateValidator.CertificateValidatorBuilder();
            return builder
                    .session(session)
                    .keyUsage()
                        .parse(config.getKeyUsage())
                    .extendedKeyUsage()
                        .parse(config.getExtendedKeyUsage())
                    .revocation()
                        .cRLEnabled(config.getCRLEnabled())
                        .cRLDPEnabled(config.getCRLDistributionPointEnabled())
                        .cRLrelativePath(config.getCRLRelativePath())
                        .oCSPEnabled(config.getOCSPEnabled())
                        .oCSPResponseCertificate(config.getOCSPResponderCertificate())
                        .oCSPResponderURI(config.getOCSPResponder());
        }
    }*/
	
    /*
    protected static class UserIdentityToModelMapperBuilder {

        static UserIdentityToModelMapper fromConfig(AfirmaAuthenticatorConfigModel config) {

            AfirmaAuthenticatorConfigModel.IdentityMapperType mapperType = config.getUserIdentityMapperType();
            String attributeName = config.getCustomAttributeName();

            UserIdentityToModelMapper mapper = null;
            switch (mapperType) {
                case USER_ATTRIBUTE:
                    mapper = UserIdentityToModelMapper.getUserIdentityToCustomAttributeMapper(attributeName);
                    break;
                case USERNAME_EMAIL:
                    mapper = UserIdentityToModelMapper.getUsernameOrEmailMapper();
                    break;
                default:
                    logger.warnf("[UserIdentityToModelMapperBuilder:fromConfig] Unknown or unsupported user identity mapper: \"%s\"", mapperType.getName());
            }
            return mapper;
        }
    }*/
    
	protected X509Certificate[] getCertificateChain(AuthenticationFlowContext context) {
        try {
            // Get a x509 client certificate
            X509ClientCertificateLookup provider = context.getSession().getProvider(X509ClientCertificateLookup.class);
            if (provider == null) {
                logger.errorv("\"{0}\" Spi is not available, did you forget to update the configuration?",
                        X509ClientCertificateLookup.class);
                return null;
            }

            X509Certificate[] certs = provider.getCertificateChain(context.getHttpRequest());

            if (certs != null) {
                for (X509Certificate cert : certs) {
                    logger.tracev("\"{0}\"", cert.getSubjectDN().getName());
                }
            }

            return certs;
        }
        catch (GeneralSecurityException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }
	
    protected Response createInfoResponse(AuthenticationFlowContext context, String infoMessage, Object ... parameters) {
        LoginFormsProvider form = context.form();
        return form.setInfo(infoMessage, parameters).createInfoPage();
    }
       
	@Override
	public void close() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void authenticate(AuthenticationFlowContext context) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void action(AuthenticationFlowContext context) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean requiresUser() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean configuredFor(KeycloakSession session, RealmModel realm, UserModel user) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setRequiredActions(KeycloakSession session, RealmModel realm, UserModel user) {
		// TODO Auto-generated method stub
		
	}

}
