# Autenticació amb certificats contra @firma a keycloak

Mòdul d'autenticació amb certificats x509 validats contra el servei d'@firma.

Versió que empra el plugin del GOIB/Fundació BIT per tal d'accedir als serveis de validació de certificats de @firma.

## Implementacions

### Implementació inicial

Cridada al serveis web de @firma emprant les classes stub generades amb wsconsume de jboss EAP 7.2 @ToniSánchezMagraner.

Per a la generació de les classes stub al projecte inicial s'ha emprat l'eina wsconsume de jboss EAP 7.2:
* ./wsconsume.sh -k -p es.caib.autenticacio.afirma ~/DSSAfirmaVerifyCertificate.wsdl -w https://afirmapre.caib.es/afirmaws/services/DSSAfirmaVerifyCertificate?wsdl -o ~/
* ./wsconsume.sh -k -n -p es.caib.autenticacio.afirma ~/DSSAfirmaVerifyCertificate.wsdl -o ~/
* ./wsconsume.sh -p es.caib.autenticacio.afirma ~/DSSAfirmaVerifyCertificate.wsdl -o ~/


### Implementació emprant pluginsib: validatecertificate

Cridada al servei web de @firma emprant el plugin del GOIB: https://github.com/GovernIB/pluginsib-validatecertificate @SalvadorAntich

## Instal·lació del mòdul a keycloak

Depenent de la versió de keycloak es podrà instal·lar el mòdul desplegant el mòdul (jar) al propi jboss que executa keycloak, per exemple copiant-lo al directory deployments (provat amb la versió 15 de keycloak).

En cas que disposem de versions antigues de keycloak es podrà instal·lar com a mòdul, amb l'estructura descrita al directori *setup*.
```
$JBOSS_HOME\modules\system\layers\keycloak\es\caib\autenticacio\afirma\main
                                                                        |__ module.xml
                                                                        |__ es.caib.afirmaauth.jar

```

## Configuració amb proxy

En cas que el keycloak estigui publicat a través d'un proxy apache, aquest s'ha de configurar per sol·licitar, de manera opcional, el certificat a l'usuari (autenticació mútua TLS: *SSLVerifyClient optional*) i passar les dades del certificat de l'usuari a la capçalera HTTP, per tal que arribin al jboss/keycloak.

Al directory *setup* es pot trobar el fitxer de configuració d'apache complet: *ssl-login.conf*.

Per configurar l'autenticació mútua es necessitarà un fitxer pem que contengui els certificats públics arrel de totes les entitats emisores de certificats admesos per @firma (aquest fitxer es pot extreure de la instal·lació de @firma).

Addicionalment també s'haurà d'instal·lar la cadena pública dels certificats per on està el proxy al cacerts del JDK que s'empra per executar keycloak.

## Configuració interna de keycloak

Una vegada instal·lat el mòdul a keycloak, s'ha de crear un nou authenticator i configurar-lo per tal d'activar l'autenticació amb certificat.


## Guia d'ús de keycloak amb docker:

Llistar imatges
`$ docker image list`

Llistar contenidors
`$ docker container list`

Accedir al bash d'un contenidor
`$ docker exec -t -i <containerID> /bin/bash`

Crear una nova imatge a partir d'un fitxer de definició
`$ docker build --tag keycloak/toni . --file keycloak-afirma-docker-composer.dck`

Arrancar keycloak via docker:
`$ docker run -p 8080:8080 -p 9990:9990 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin keycloak/toni:latest`

Afegir el mòdul d'autenticació amb certificat amb validació contra @firma (via jboss-cli.sh)

```
$ ./jboss-cli.sh
[ ] connect
[ ] module add --name=es.caib.autenticacio.afirma --resources=es.caib.afirmaauth.jar --dependencies=org.keycloak.keycloak-core,org.keycloak.keycloak-server-spi,org.keycloak.keycloak-server-spi-private,org.keycloak.keycloak-services,org.jboss.logging,org.jboss.ws.api,org.jboss.resteasy.resteasy-jaxrs,javax.ws.rs.api,javax.jws.api,javax.xml.ws.api,javax.servlet.api
```

Per desplegar un mòdul
`[ ] deployment deploy-file /tmp/es.caib.afirmaauth.jar`


## Enllaços que han estat útils en el desenvolupament:

* https://www.baeldung.com/java-keycloak-custom-user-providers
* https://stackoverflow.com/questions/57778240/noclassdeffounderror-in-a-provider-jar-when-using-a-class-from-org-keycloak-auth
* https://github.com/GovernIB/maven
* http://tutorials.jenkov.com/maven/maven-build-fat-jar.html
* https://stackoverflow.com/questions/574594/how-can-i-create-an-executable-jar-with-dependencies-using-maven

Solució de l'error "Could not find wsdl:binding operation info for web method ValidarCertificado": Afegint la dependència org.jboss.ws.cxf.jbossws-cxf-client al MANIFEST.MF
* https://stackoverflow.com/questions/10045006/binding-fails-for-new-soap-request-in-existing-project/10117596

   